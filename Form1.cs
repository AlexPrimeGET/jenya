﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoodBase
{
    public partial class Form1 : Form
    {
        FoodBaseDbEntities db;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            db = new FoodBaseDbEntities();

            dataGridView1.DataSource = db.Products.AsEnumerable()
                .Select(x => new ProductViewModel(x))
                .ToList();
            dataGridView2.DataSource = db.Carts.AsEnumerable()
                .Select(x => new CartViewModel(x))
                .ToList();

            dataGridView1.AutoResizeColumns();
            dataGridView2.AutoResizeColumns();

            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "Id";
            List<Type> types = db.Types.ToList();
            types.Insert(0, new Type() { Id = 0, Name = "Все" });
            comboBox1.DataSource = types;

            comboBox2.DisplayMember = "Name";
            comboBox2.ValueMember = "Id";
            List<Food> foods = db.Foods.ToList();
            foods.Insert(0, new Food() { Id = 0, Name = "Все" });
            comboBox2.DataSource = foods;
            comboBox2.Visible = true;
        }

        //First filter
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int typeId = (int)comboBox1.SelectedValue;

            if (typeId != 0)
            {
                dataGridView1.DataSource = db.Products
                    .Where(c => c.Food.Type.Id == typeId)
                    .AsEnumerable()
                    .Select(c => new ProductViewModel(c))
                    .ToList();

                comboBox2.Visible = true;
                List<Food> foods = db.Foods
                    .Where(eq => eq.TypeId == typeId)
                    .ToList();

                foods.Insert(0, new Food() { Id = 0, Name = "Все" });
                comboBox2.DataSource = foods;
            }
            else
            {
                dataGridView1.DataSource = db.Products
                    .AsEnumerable()
                    .Select(c => new ProductViewModel(c))
                    .ToList();

                comboBox2.Visible = false;
            }

            dataGridView1.AutoResizeColumns();
        }

        //Second filter
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int typeId = (int)comboBox1.SelectedValue;
            int equipmentId = (int)comboBox2.SelectedValue;
            if (equipmentId != 0)
            {
                dataGridView1.DataSource = db.Products
                    .Where(c => c.Food.Type.Id == typeId && c.Food.Id == equipmentId)
                    .AsEnumerable().Select(c => new ProductViewModel(c))
                    .ToList();
            }
            else if (typeId != 0)
            {
                dataGridView1.DataSource = db.Products
                    .Where(c => c.Food.Type.Id == typeId)
                    .AsEnumerable().Select(c => new ProductViewModel(c))
                    .ToList();
            }
            dataGridView1.AutoResizeColumns();
        }


        //Add product to cart
        private void button1_Click(object sender, EventArgs e)
        {
            var cell = dataGridView1.CurrentCell;
            if (cell != null)
            {
                var row = cell.OwningRow;
                int productId = (int)row.Cells["Id"].Value;
                int selectedCount;
                if (int.TryParse(textBox1.Text, out _))
                {
                    selectedCount = int.Parse(textBox1.Text);
                }
                else
                {
                    selectedCount = 1;
                }
                var product = db.Products.FirstOrDefault(c => c.Id == productId);
                if (product.Quantity >= selectedCount)
                {
                    var exCart = db.Carts.FirstOrDefault(c => c.ProductId == productId);
                    if (exCart == null)
                    {
                        Cart cart = new Cart()
                        {
                            ProductId = product.Id,
                            Quantity = selectedCount
                        };

                        db.Carts.Add(cart);
                        product.Quantity -= selectedCount;

                    }
                    else
                    {
                        exCart.Quantity += selectedCount;
                        product.Quantity -= selectedCount;
                    }
                    db.SaveChanges();

                    dataGridView1.DataSource = db.Products.AsEnumerable()
                        .Select(c => new ProductViewModel(c))
                        .ToList();
                    dataGridView2.DataSource = db.Carts.AsEnumerable()
                        .Select(c => new CartViewModel(c))
                        .ToList();

                    dataGridView1.AutoResizeColumns();
                    dataGridView2.AutoResizeColumns();
                }
            }
        }

        //Delete products from cart
        private void button2_Click(object sender, EventArgs e)
        {
            var cell = dataGridView2.CurrentCell;
            if (cell != null)
            {
                var row = cell.OwningRow;
                int id = (int)row.Cells["Id"].Value;

                var cart = db.Carts.FirstOrDefault(c => c.Id == id);
                var product = db.Products.FirstOrDefault(c => c.Id == cart.ProductId);

                product.Quantity += cart.Quantity;

                db.Carts.Remove(cart);

                db.SaveChanges();

                dataGridView1.DataSource = db.Products.AsEnumerable()
                        .Select(c => new ProductViewModel(c))
                        .ToList();
                dataGridView2.DataSource = db.Carts.AsEnumerable()
                    .Select(c => new CartViewModel(c))
                    .ToList();

                dataGridView1.AutoResizeColumns();
                dataGridView2.AutoResizeColumns();
            }
        }

        //Add product
        private void button3_Click(object sender, EventArgs e)
        {
            string typeName = textBox2.Text;
            string foodName = textBox3.Text;
            decimal price = decimal.Parse(textBox4.Text);
            int count = int.Parse(textBox5.Text);

            var type = db.Types.FirstOrDefault(m => m.Name == typeName);
            //New type if no there
            if (type == null)
            {
                type = new Type()
                {
                    Name = typeName
                };
                db.Types.Add(type);
            }
            //New food if no there
            var food = db.Foods.FirstOrDefault(m => m.Name == foodName);
            if (food == null)
            {
                food = new Food()
                {
                    Type = type,
                    Name = foodName,
                    Price = price
                };
                db.Foods.Add(food);
            }

            var product = new Product()
            {
                Food = food,
                Quantity = count
            };
            db.Products.Add(product);
            db.SaveChanges();

            //Update table
            dataGridView1.DataSource = db.Products.AsEnumerable()
                        .Select(c => new ProductViewModel(c))
                        .ToList();

            dataGridView1.AutoResizeColumns();

            List<Type> types = db.Types.ToList();
            types.Insert(0, new Type() { Id = 0, Name = "Все" });
            comboBox1.DataSource = types;
        }

        //Delete product
        private void button4_Click(object sender, EventArgs e)
        {
            var cell = dataGridView1.CurrentCell;
            if (cell != null)
            {
                var row = cell.OwningRow;
                int id = (int)row.Cells["Id"].Value;

                Product product = db.Products.Find(id);
                db.Products.Remove(product);
                db.SaveChanges();

                //Update table
                dataGridView1.DataSource = db.Products
                    .AsEnumerable()
                    .Select(c => new ProductViewModel(c))
                    .ToList();
                dataGridView2.DataSource = db.Carts
                    .AsEnumerable()
                    .Select(c => new CartViewModel(c))
                    .ToList();

                dataGridView1.AutoResizeColumns();
                dataGridView2.AutoResizeColumns();

                List<Type> types = db.Types.ToList();
                types.Insert(0, new Type() { Id = 0, Name = "Все" });
                comboBox1.DataSource = types;
                List<Food> foods = db.Foods.ToList();
                foods.Insert(0, new Food() { Id = 0, Name = "Все" });
                comboBox2.DataSource = foods;
            }
        }

        //Edit product
        private void button5_Click(object sender, EventArgs e)
        {
            var cell = dataGridView1.CurrentCell;
            if (cell != null)
            {
                var row = cell.OwningRow;
                int id = (int)row.Cells["Id"].Value;

                decimal price = decimal.Parse(textBox6.Text);
                int count = int.Parse(textBox7.Text);

                Product product = db.Products.Find(id);

                product.Food.Price = price;
                product.Quantity = count;
                db.SaveChanges();

                //Update table
                dataGridView1.DataSource = db.Products.AsEnumerable()
                        .Select(c => new ProductViewModel(c))
                        .ToList();

                dataGridView1.AutoResizeColumns();
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            var cell = dataGridView1.CurrentCell;
            if (cell != null)
            {
                var row = cell.OwningRow;

                decimal price = (decimal)row.Cells["Price"].Value;
                int count = (int)row.Cells["Quantity"].Value;

                textBox6.Text = price.ToString();
                textBox7.Text = count.ToString();
            }
        }
    }
}

