﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodBase
{
    public class CartViewModel
    {
        public int Id { get; set; }

        [DisplayName("Тип")]
        public string Type { get; set; }

        [DisplayName("Еда")]
        public string Food { get; set; }

        [DisplayName("Цена")]
        public decimal Price { get; set; }

        [DisplayName("Количество")]
        public int Quantity { get; set; }

        [DisplayName("Сумма")]
        public decimal Sum { get; set; }

        public CartViewModel(Cart prod)
        {
            Id = prod.Id;
            Type = prod.Product.Food.Type.Name;
            Food = prod.Product.Food.Name;
            Price = prod.Product.Food.Price;
            Quantity = prod.Quantity;
            Sum = prod.Product.Food.Price * prod.Quantity;
        }
    }
}
