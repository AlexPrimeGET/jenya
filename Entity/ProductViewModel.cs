﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodBase
{
    public class ProductViewModel
    {
        public int Id { get; set; }

        [DisplayName("Тип")]
        public string Type { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Цена")]
        public decimal Price { get; set; }

        [DisplayName("Количество")]
        public int Quantity { get; set; }

        public ProductViewModel(Product product)
        {
            Id = product.Id;
            var food = product.Food;
            Type = food.Type.Name;
            Name = food.Name;
            Price = food.Price;
            Quantity = product.Quantity;
        }
    }
}
